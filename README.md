# Comparte Ride

Comparte Ride was a carpooling platform developed on 2016 as
an alternative for Mexico City inhabitants during an [air pollution
alert](https://phys.org/news/2016-03-mexico-city-air-pollution.html)
where the car usage was restricted and over 1.1 million cars
were banned from the streets.

## Development

To start working on this project I highly recommend you to check
[pydanny's](https://github.com/pydanny) [Django Cookiecutter](https://github.com/pydanny/cookiecutter-django) [documentation](https://cookiecutter-django.readthedocs.io/en/latest/developing-locally-docker.html) on how to get this project up and running locally.
If you don't want to do so, just run:

```bash
docker-compose -f local.yml build
docker-compose -f local.yml up
```

## Contributing

I'll be happily accepting pull requests from anyone.

This that are missing right now:

* [ ] Add tests and coverage implementations
* [ ] Remove weak Token Authorization system
* [ ] Implement more async and periodic tasks to improve the rating system
* [ ] A UI!



